zynaddsubfx (3.0.3-1) unstable; urgency=medium

  * New upstream version 3.0.3
  * Patch refreshed.
  * Vcs - use git instead of cgit.
  * Bump Standards.
  * Use secure uri where possible.
  * Introduce postclone.sh script to ignore .pc/ dir.
  * Remove trailing-whitespaces.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sat, 09 Dec 2017 21:55:18 +0100

zynaddsubfx (3.0.2-1) unstable; urgency=medium

  * New upstream version 3.0.2
  * Patch applied upstream.
  * Bump Standards.
  * Sign tags.
  * Update copyright file.
  * Avoid useless linking.
  * Add overrides files.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 24 Jul 2017 00:42:30 +0200

zynaddsubfx (3.0.1-1) unstable; urgency=medium

  * Remove d/zynaddsubfx.1 file.
  * New upstream version 3.0.1
  * Patch refreshed.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 01 Dec 2016 19:11:49 +0100

zynaddsubfx (3.0.0-2) unstable; urgency=medium

  * Install upstream man pages. (Closes: #845355)

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 23 Nov 2016 01:13:03 +0100

zynaddsubfx (3.0.0-1) unstable; urgency=medium

  * New upstream version 3.0.0 (Closes: #832988) (Closes: #832989)
  * Patches refreshed/removed.
  * Set dh/compat 10.
  * Add libgl1-mesa-dev as B-D.
  * Drop dbg package in favour of dbgsym.
  * Update copyright file. (Closes: #823561)
  * Add spelling patch.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 21 Nov 2016 07:10:15 +0100

zynaddsubfx (2.5.4-2) unstable; urgency=medium

  * Fix VCS fields.
  * Add patch to fix build on armel and armhf.
  * Disable all neon flags.
  * Add patch to fix test on big-endian systems.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 16 Jun 2016 03:57:05 +0200

zynaddsubfx (2.5.4-1) unstable; urgency=medium

  * Imported Upstream version 2.5.4
  * Remove patches applied upstream.
  * Bump Standards.
  * Fix VCS fields.
  * Fix hardening.
  * Add spelling patch.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 15 Jun 2016 08:42:23 +0200

zynaddsubfx (2.5.2-2) unstable; urgency=medium

  * Fix copyright file.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Wed, 02 Dec 2015 00:26:43 +0100

zynaddsubfx (2.5.2-1) unstable; urgency=medium

  [ Jaromír Mikeš ]
  * Imported Upstream version 2.5.2
  * Patches refreshed.
  * FAQ.txt removed upstream.
  * Add patch to fix build.
  * Install upstream desktop files and icon.
  * Removed menu file (in response to the tech-ctte decision on #741573).
  * Add Keywords to desktop files.
  * Add another spelling patch.
  * Update copyright file.
  * Remove debian/desktop file.
  * Introduce zynaddsubfx-data package.

  [ Sebastian Ramacher ]
  * debian/control: Bump versions in Breaks + Replaces.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Mon, 23 Nov 2015 21:39:33 +0100

zynaddsubfx (2.5.1-3) unstable; urgency=medium

  * Added patch to fix build on armel arch.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 29 Sep 2015 22:41:40 +0200

zynaddsubfx (2.5.1-2) unstable; urgency=medium

  * Add patch to fix build on some archs.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Tue, 08 Sep 2015 23:03:32 +0200

zynaddsubfx (2.5.1-1) unstable; urgency=medium

  * New upstream release.
  * Update watch file to be more flexible.
  * Update build deps.
  * Update docs file.
  * Refresh patches

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sun, 05 Jul 2015 00:41:20 +0200

zynaddsubfx (2.4.3-6) unstable; urgency=medium

  * Fix install on 32bit systems.(Closes: #789345)

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sat, 20 Jun 2015 08:39:41 +0200

zynaddsubfx (2.4.3-5) unstable; urgency=medium

  * Team upload.
  * Don't ship anything in /usr/lib64. (Closes: #787919)
  * Avoid shipping upstream changelog twice.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Sun, 07 Jun 2015 00:44:45 +0100

zynaddsubfx (2.4.3-4) unstable; urgency=low

  * Bump Standards.
  * Add VCS canonical URLs.

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 09 May 2013 16:02:36 +0200

zynaddsubfx (2.4.3-3) experimental; urgency=low

  * Team upload.
  * Stop forcing SSE and x86 compiler options in the generic target.
  * Add patch to fix small misspellings.
  * Enable parallel builds.

 -- Alessio Treglia <alessio@debian.org>  Tue, 27 Nov 2012 01:39:14 +0000

zynaddsubfx (2.4.3-2) experimental; urgency=low

  * Added dssi-dev and liblo-dev as dependency
  * Introduced zynaddsubfx-dssi package (Closes: #629194)

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Sat, 01 Sep 2012 12:17:59 +0200

zynaddsubfx (2.4.3-1) experimental; urgency=low

  [ Jaromír Mikeš ]
  * New upstream release
  * Taking over package with agreement of Eduardo Marcel.
     <macan@debian.org>
  * Set priority optional
  * Add cmake as build dependency
  * Set format 3.0
  * Add gitignore file
  * Delete substvars file
  * Add gbp.conf file
  * Switch to short debhelper
  * Set compat to 9 to fix hardening
  * Set versions of cmake and debhelper to fix hardening
  * Remove quilt from build dependencies
  * Bump Standards
  * Added dbg package
  * Added zynaddsubfx.manpages file
  * Added zynaddsubfx.install file
  * Added misc:Depends to control file
  * Deleted old patches

  [ Alessio Treglia ]
  * Update copyright file

 -- Jaromír Mikeš <mira.mikes@seznam.cz>  Thu, 23 Aug 2012 14:14:21 +0200

zynaddsubfx (2.4.0-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * debian/patches/{05_partui,06_bankui,09_fluid_1.3}.patch:
    - Fluid 1.3.x no longer produces empty implementations for totally
      empty functions in .fl files, fix FTBFS (Closes: #633478)

 -- Alessio Treglia <alessio@debian.org>  Sun, 21 Aug 2011 10:51:06 +0200

zynaddsubfx (2.4.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Drop LASH support (Closes: #627420):
    - Remove liblash-dev from Build-Depends field.
    - Disable 016_lash.patch patch.

 -- Alessio Treglia <alessio@debian.org>  Thu, 02 Jun 2011 16:28:21 +0200

zynaddsubfx (2.4.0-1) unstable; urgency=low

  * Built with LASH support (Closes: #486243)
  * Build-depends upon libjack-dev (Closes: #527443)
  * Removed patches that were incorporated upstream
  * New upstream release (Closes: #543557)

 -- Eduardo Marcel Maçan <macan@debian.org>  Thu, 20 Aug 2009 00:39:47 -0300

zynaddsubfx (2.2.1-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * Change fftw3-dev build-dep to libfftw3-dev (Closes: #445792)
  * Add quilt patching system
  * Make clean not ignore errors in rules
  * Remove unused debhelper commands in rules
  * debian/menu - Change section Apps/Sound to Sound
  * Add watch file
  * Add Homepage field in control
  * Remove deprecated encoding field from desktop file
  * 01_kfreebsd_fix.patch (Closes: #415675)
    + src/Makefile.inc - fall back to OSS on kfreebsd
  * Move previous source changes to patches
    + 015_oss_and_jack.patch - Use OSS_AND_JACK, not just OSS
    + 02_makefile.patch - Add install target
    + 03_main.patch - Add noui as a parameter to initprogram()
    + 04_virkeyboard.patch - Warning and crash caused in virtual keyboard
      + patch by Guus Sliepen
    + 05_partui.patch - Add PartUI_() function
    + 06_bankui.patch - Add BankProcess_() function
    + 07_ossaudiooutput.patch - Big Endian Fix
    + 08_jackaudiooutput.patch - Add jack ports
    + 09_envelopeparams.patch - Set realtime priority accordingly
    + 10_LFO.patch - Improve frequency randomizer
  * Bump debhelper build-dep and compat to 5
  * Bump standards version to 3.7.3

 -- Barry deFreese <bddebian@comcast.net>  Sun, 16 Dec 2007 17:59:25 -0500

zynaddsubfx (2.2.1-4) unstable; urgency=low

  * Added zlib1g-dev to the build dependencies (closes: #352014)

 -- macan <macan@debian.org>  Fri, 10 Feb 2006 13:15:38 -0200

zynaddsubfx (2.2.1-3) unstable; urgency=low

  * Patched zynaddsubfx to connect to the default jack output
  * Fixed some compilation warnings and crashing caused by a bug in virtual keyboard, patch by Guus Sliepen (closes: #350152)
  * Updated .desktop file to be XDG compliant. Patch by Free Ekanayaka (closes: #313368)
  * Set realtime priority acordingly. Patch by Ben Hutchings (closes: #314671)

 -- Eduardo Marcel Macan <macan@debian.org>  Wed,  8 Feb 2006 08:50:06 -0200

zynaddsubfx (2.2.1-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * build-depend on libjack0.100.0-dev. Closes: #317229.

 -- Andreas Barth <aba@not.so.argh.org>  Sun, 31 Jul 2005 13:56:28 +0200

zynaddsubfx (2.2.1-2) unstable; urgency=high

  * Added noui as a parameter to initprogram() (closes: #307522)
  * Fixed issues introduced by Brossier's patch (#306824) on little endian architectures (closes: #309401)

 -- Eduardo Marcel Macan <macan@debian.org>  Tue, 17 May 2005 11:29:38 -0300

zynaddsubfx (2.2.1-1) unstable; urgency=low

  * Gave proper credit in this changelog to people who sent patches
  * New upstream release

 -- Eduardo Marcel Macan <macan@debian.org>  Fri, 29 Apr 2005 09:29:12 -0300

zynaddsubfx (2.2.0-2) unstable; urgency=low

  * Fixed endianess problem thanks Paul Brossier (closes: #306824)

 -- Eduardo Marcel Macan <macan@debian.org>  Fri, 29 Apr 2005 09:17:20 -0300

zynaddsubfx (2.2.0-1) unstable; urgency=low

  * New upstream release
  * Fixed XMLWrapper so that saved parameters are loaded properly

 -- Eduardo Marcel Macan <macan@debian.org>  Sat, 23 Apr 2005 08:18:08 -0300

zynaddsubfx (2.1.1-1.1) unstable; urgency=low

  * NMU
  * Changes src/UI/PresetsUI.fl to include FL/fl_ask.H instead of
    FL/fl_ask.h (closes: #304125)
  * Disabled non portable float to int assembly conversion in
    src/Makefile.inc (ASM_F2I=NO)

 -- Paul Brossier <piem@altern.org>  Wed, 13 Apr 2005 03:54:45 +0100

zynaddsubfx (2.1.1-1) unstable; urgency=low

  * menu file now references zyn's icon (closes: #299412)
  * New upstream release (closes: #269004)

 -- Eduardo Marcel Macan <macan@debian.org>  Tue, 28 Dec 2004 20:50:04 -0200

zynaddsubfx (1.4.3-1.1) unstable; urgency=low

  * NMU
  * recompile with libjack0.80.0-dev (closes: #228790)

 -- Junichi Uekawa <dancer@debian.org>  Wed, 28 Jan 2004 06:50:09 +0900

zynaddsubfx (1.4.3-1) unstable; urgency=low

  * New upstream release

 -- Eduardo Marcel Macan <macan@debian.org>  Wed,  3 Sep 2003 16:05:56 -0300

zynaddsubfx (1.4.2-2) unstable; urgency=low

  * Fixed build dependencies (closes: #203424)

 -- Eduardo Marcel Macan <macan@debian.org>  Thu,  7 Aug 2003 08:54:35 -0300

zynaddsubfx (1.4.2-1) unstable; urgency=low

  * New upstream release

 -- Eduardo Marcel Macan <macan@debian.org>  Thu, 17 Jul 2003 21:54:46 -0300

zynaddsubfx (1.4.1-1) unstable; urgency=low

  * Initial Release.

 -- Eduardo Marcel Macan <macan@debian.org>  Thu, 19 Jun 2003 23:33:18 -0300

